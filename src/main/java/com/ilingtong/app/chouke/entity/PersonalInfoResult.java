package com.ilingtong.app.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/5/16.
 * mail: wuqian@ilingtong.com
 * Description:7001接口返回json结构
 */
public class PersonalInfoResult extends BaseResult implements Serializable {
    private UserInfo body;

    public UserInfo getBody() {
        return body;
    }

    public void setBody(UserInfo body) {
        this.body = body;
    }
}
